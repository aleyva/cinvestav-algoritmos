Deber´as programar y utilizar en tu experimento funciones para insertar,
eliminar, buscar y hacer recorridos en orden tanto en un BST como en un RBT

#include <stdio.h>

struct s_node{
    int key;
    struct s_node *p;
    struct s_node *left;
    struct s_node *right
}

class BST(){
    public:
        struct s_node root;
        print_in_order(struct s_node *node);
        print_in_preorder(struct s_node *node);
        print_in_preorder(struct s_node *node);
        tree_seach(struct s_node *node, int key);
        iterative_tree_seach(struct s_node *node, int key);
}

class RBT : public BST{
    public:        
        tree_insert(struct s_node *node, struct s_node new_node){   
        transplant(struct s_node *node, struct s_node old, struct s_node trans){
        tree_delete(struct s_node *node, z){
   
}

void BTS::print_in_order(struct s_node *node){
    if(node!=NULL){
        in_order_walk_tree(node->left);
        std::cout << node->key << std::endl;        
        in_order_walk_tree(node->right);
    }
}

void BTS::print_in_preorder(struct s_node *node){
    if(node!=NULL){
        std::cout << node->key << std::endl;        
        in_order_walk_tree(node->left);       
        in_order_walk_tree(node->right);
    }
}

void BTS::print_in_posorder(struct s_node *node){
    if(node!=NULL){
        in_order_walk_tree(node->left);      
        in_order_walk_tree(node->right);
        std::cout << node->key << std::endl;        
    }
}

struct node BTS::tree_seach(struct s_node *node, int key){
    if(node==NULL || node->key==key)
        return node;
    if(key < node->key)
        return tree_search(node->left, key);
    return tree_seach(node->right, key);
}

struct node BTS::iterative_tree_seach(struct s_node *node, int key){
    while(node!=NULL || node->key==key){}
        if(key<=node->key)
            node = node->left;
        else
            node = node->right;
    }
    return node;
}

struct node RBT::tree_minimum(struct s_node *node){
    if(node!=NULL)
        while(node->left!=NULL)
            node = node->left;
    return node;
}


struct node RBT::tree_succesor(struct s_node *node)
    if(node!=NULL){
        if(node.rigth != NULL)
            return tree_minimun(node->right)
        struct s_node *previous = node->p;
        While(y != NULL and y->right==node){}
            node = previous;
            previous = previous->p;
        }
    }
    return previous;

void BTS::tree_insert(struct s_node *node, struct s_node new_node){  
    struct s_node *previous = NULL;
    struct s_node *actual = node.root;
    while(actual!=null)        
        previous=actual;
        if(new_node->key <= actual->key)            
            actual = actual->left;
        else
            actual = actual->right;
    new_node->p = previous
    if(previous == null)
        node = new_node;    //root = new_node
    else{
        if (previous->key <= previous->key)            
            previous->left = previous;
        else:
            previous->rigth = new_node;
    }
}
    


void RBT::transplant(struct s_node *node, struct s_node old, struct s_node trans){
    if(old->p == NULL) //no hay raiz
        //node.root = vars
    else{
        if (old->p->left == s_node) //remplaza con hijo izquierdo
            old->p->left = new_node;
        else
            old->p->right = new_node; //remplaza con hijo derecho
    }
    
    if(new_node != NULL)   
        new_node->p = old->p;
}


void RBT::tree_delete(struct s_node *node, z){
    if (z->left == NULL) //no hay hijo izquierdo
        transplant(node,z,z->right);
    else{
        if (z->right == NULL) //no hay hijo derecho
            transplant(node,z,z->left)
        else{
            y = tree_minimum(z->right) //El minimo remplazará a z
            if(z->right != y)    //el minimo no esta en z->right
                transplant(node,y,y->right)
                y->right = z->right
                y->right.p = y
            transplant(node,z,y)   //el minimo esta en z->right
            y->left = z->left
            z->left.p = y
        }
    }
}
