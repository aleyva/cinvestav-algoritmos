Para com pilar
make

Para ejecutar
Al programa le deben ser introducidos 3 parám etros
• tipo_de_arbol: Es el tipo de arbol que se desea ejecutar, los valores pueden ser ‘BST’ o ‘RBT’
• num_elementos: Número de elementos a insertar en el árbol, debe ser un numero natural.
• max_num: Determina el rango de las llaves a introducir en el árbol, por ejemplo, si se introduce 10, valor _ llaves ∈ [0,9], por lo que los números a introducir en el árbol solo podrán ser [0,1,2,...9].

Ejemplo de ejecución
./output.out BST 125000 125;
Se construye un árbol BST e introduce 125000 elementos, donde el rango de las llaves
va de [0,..., 124]. Debido a que el rango de elementos es muy limitado, el árbol que
construye esta ejecución es casi lineal.

Otros ejemplos de ejecución
./output.out BST 250000 2500;
./output.out RBT 500000 500000;