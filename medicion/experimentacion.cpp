#include <iostream>
#include <stdio.h> 
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include <string.h>

#define BLANK 6 
#define uint32 long long


struct s_node{
    uint32 key;
    char color;     // 'r' for red, 'b' for black
    s_node *p;
    s_node *left;
    s_node *right;
};

class BST{
    public:
        BST(){ root = NULL; }
        void init();          
        void print(int option);                
        void search(int key);        
        void insert(int key);
        void delete_(int key);       
        void delete_all();  
        
    protected:    
        struct s_node *root;      
        void _print2D_in_order(struct s_node *node, int space);    
        void _print_in_order(struct s_node *node);
        void _print_in_preorder(struct s_node *node);
        void _print_in_posorder(struct s_node *node);
        struct s_node* _new_node(int key);
        struct s_node* _minimum(struct s_node *node);
        struct s_node* _successor(struct s_node *node);
        void _transplant(struct s_node *node, struct s_node *old, struct s_node *trans);        
        struct s_node* _search(struct s_node *node, int key);
        struct s_node* _iterative_search(struct s_node *node, int key);
        void _insert(struct s_node *node, struct s_node *new_node);
        void _delete(struct s_node *node, struct s_node *z); 
        void _delete_all(struct s_node *node);  
};

class RBT : public BST{
    public:        
        void rbt_insert(int key);
        void rbt_delete(int key);                
    private:                
        void _left_rotate(struct s_node *node, struct s_node *x);
        void _right_rotate(struct s_node *node, struct s_node *x);
        void _insert_fixup(struct s_node *node, struct s_node *z);
        void _delete_fixup(struct s_node *node, struct s_node *x);
        void _rbt_insert(struct s_node *node, struct s_node *new_node);        
        void _rbt_delete(struct s_node *node, struct s_node *delete_node);        

};


//---------------------------------------
//---------------------------------------
//---------------------------------------
//           BINARY SEARCH TREE
//---------------------------------------
//---------------------------------------
//---------------------------------------

void BST::_print2D_in_order(struct s_node *node, int spaces){ 
    if (node != NULL){        
        spaces += BLANK;             
        _print2D_in_order(node->right, spaces);                      
        for (int i = BLANK; i < spaces; i++) 
            std::cout << " ";
        std::cout << node->key;
        if(node->color != 'n')
            std::cout << ' ' << node->color;
        std::cout << "---------------------" << std::endl;        
        _print2D_in_order(node->left, spaces); 
    }
} 

void BST::_print_in_order(struct s_node *node){
    if(node != NULL){
        _print_in_order(node->left);
        std::cout << node->key;
        if(node->color != 'n')
            std::cout << node->color;               
        std::cout << ", "; 
        _print_in_order(node->right);
    }
}

void BST::_print_in_preorder(struct s_node *node){
    if(node != NULL){
        std::cout << node->key;
        if(node->color != 'n')
            std::cout << node->color;               
        std::cout << ", ";          
        _print_in_preorder(node->left);        
        _print_in_preorder(node->right);
    }
}

void BST::_print_in_posorder(struct s_node *node){
    if(node != NULL){
        _print_in_posorder(node->left);        
        _print_in_posorder(node->right);
        std::cout << node->key;
        if(node->color != 'n')
            std::cout << node->color;               
        std::cout << ", ";         
    }
}

struct s_node* BST::_new_node(int key){ 
    struct s_node *node = new s_node;
    node->key = key; 
    node->color = 'n';
    node->left = node->right = node->p = NULL; 
    return node; 
} 

struct s_node* BST::_minimum(struct s_node *node){
    if(node!=NULL)
        while(node->left!=NULL)
            node = node->left;
    return node;
}

struct s_node* BST::_successor(struct s_node *node){
    struct s_node *previous = NULL;
    if(node!=NULL){
        if(node->right != NULL)
            return _minimum(node->right);
        struct s_node *previous = node->p;
        while(previous != NULL and previous->right==node){
            node = previous;
            previous = previous->p;
        }
    }
    return previous;
}

void BST::_transplant(struct s_node *node, struct s_node *old, struct s_node *new_node){
    if(old->p == NULL) //no hay raiz
        root = new_node;
    else{
        if (old->p->left == old) //remplaza con hijo izquierdo
            old->p->left = new_node;
        else
            old->p->right = new_node; //remplaza con hijo derecho
    }    
    if(new_node != NULL)   
        new_node->p = old->p;
}


struct s_node* BST::_search(struct s_node *node, int key){
    if(node == NULL || node->key == key)
        return node;
    if(key < node->key)
        return _search(node->left, key);
    return _search(node->right, key);
}


void BST::_insert(struct s_node *node, struct s_node *new_node){  
    struct s_node *previous = NULL;
    struct s_node *actual = node;   //this is the root
    while(actual!=NULL){        
        previous=actual;
        if(new_node->key <= actual->key)            
            actual = actual->left;
        else
            actual = actual->right;
    }
    new_node->p = previous;
    if(previous == NULL)
        root = new_node;    //root = new_node
    else{
        if (new_node->key <= previous->key)            
            previous->left = new_node;
        else
            previous->right = new_node;
    }
}

void BST::_delete(struct s_node *node, struct s_node *delete_node){
    if(delete_node->left == NULL)
        _transplant(node,delete_node,delete_node->right);
    else{
        if(delete_node->right == NULL)
            _transplant(node,delete_node,delete_node->left);
        else{
            struct s_node *min = _minimum(delete_node->right);
            if(min->p != delete_node){
                _transplant(node,min,min->right);
                min->right = delete_node->right;
                min->right->p = min;
            }
            _transplant(node,delete_node,min);
            min->left = delete_node->left;
            min->left->p = min;
        }
    }
    free(delete_node);
}

void BST::_delete_all(struct s_node *node){    
    if(node != NULL){
        _delete_all(node->left);        
        _delete_all(node->right);
        // std::cout << "   Delete: " << node->key << std::endl;
        free(node);
    }
}

void BST::print(int option){
    switch(option){
        case 6 :             
            _print2D_in_order(root, 0);    
            break;
        case 7 :
            _print_in_order(root);
            break;
        case 8 :
            _print_in_preorder(root);
            break;
        case 9 :
            _print_in_posorder(root);
            break;
    }
    std::cout << std::endl;
}

void BST::search(int key){    
    struct s_node* search_node = NULL;
    search_node = _search(root, key);
    // if(search_node!=NULL){
    //     std::cout << "   Node EXIST" << std::endl;        
    // }else{
    //     std::cout << "   Node NOT FOUND" << std::endl;
    // }
}

void BST::insert(int key){
    struct s_node *new_node;
    new_node = _new_node(key);
    // std::cout << "   Insert: " << key << std::endl;
    _insert(root,new_node);
}

void BST::delete_(int key){    
    struct s_node* delete_node = NULL;
    delete_node = _search(root, key);
    if(delete_node!=NULL){
        // std::cout << "   Delete: " << key << std::endl;
        _delete(root,delete_node);
    }else{
        // std::cout << "   Delete: " << key << "  NOT FOUND" << std::endl;
    }
}

void BST::delete_all(){
    _delete_all(root);
    root = NULL;
}

//---------------------------------------
//---------------------------------------
//---------------------------------------
//              RED BLACK TREE
//---------------------------------------
//---------------------------------------
//---------------------------------------

void RBT::_left_rotate(struct s_node *node, struct s_node *x){
    struct s_node *y = x->right;
    x->right = y->left;
    if (y->left != NULL) //traspasa la hoja izquierda a x->right
        y->left->p = x;
    //conecta correctamente con el padre
    y->p = x->p;
    if(x->p == NULL) // es la raiz
        root = y;
    else{
        if (x == x->p->left) 
            x->p->left = y;
        else
            x->p->right = y;        
    }    
    //conecta al nodo rotado
    y->left = x;
    x->p = y;
}        

void RBT::_right_rotate(struct s_node *node, struct s_node *x){
    struct s_node *y = x->left;
    x->left = y->right;
    if (y->right != NULL) //traspasa la hoja izquierda a x->left
        y->right->p = x;
    //conecta correctamente con el padre
    y->p = x->p;
    if(x->p == NULL) // es la raiz
        root = y;
    else{
        if (x == x->p->right) 
            x->p->right = y;
        else
            x->p->left = y;        
    }    
    //conecta al nodo rotado
    y->right = x;
    x->p = y;
}        

void RBT::_insert_fixup(struct s_node *node, struct s_node *z){
    while(z!=NULL && z->p!=NULL && z->p->color == 'r'){
        if(z->p->p->left == z->p){ // el nodo está en la rama izquierda
            struct s_node *uncle = z->p->p->right;
            if(uncle != NULL && uncle->color == 'r'){ //si el tio es rojo, recolorear
                z->p->color = 'b';
                uncle->color = 'b';
                z->p->p->color = 'r';
                z = z->p->p;
            }else{    // el tio es negro
                if(z == z->p->right){ //se un triangulo
                    z = z->p;
                    _left_rotate(node,z);              
                }
                //es una linea
                z->p->color = 'b';
                z->p->p->color = 'r';                
                _right_rotate(node, z->p->p);
            }
        }else{
            //lo mismo pero intercambiar right por left y viceverza
            struct s_node *uncle = z->p->p->left;
            if(uncle != NULL && uncle->color == 'r'){ //si el tio es rojo, recolorear
                z->p->color = 'b';
                uncle->color = 'b';
                z->p->p->color = 'r';
                z = z->p->p;
            }else{    // el tio es negro
                if(z == z->p->left){ //se un triangulo
                    z = z->p;
                    _right_rotate(node,z);
                }
                //es una linea
                z->p->color = 'b';
                z->p->p->color = 'r';                
                _left_rotate(node, z->p->p);
            }
        }        
    }
    //node->root->color = 'b';
    root->color = 'b';    
}

void RBT::_delete_fixup(struct s_node *node, struct s_node *x){
    while(x!=root && x->color == 'b'){
        if(x == x->p->left){ 
            struct s_node *w = x->p->right;
            if(w->color == 'r'){
                w->color = 'b';
                x->p->color = 'r';
                _left_rotate(node,x->p);
                w = x->p->right;
            }
            if(w->left->color == 'b' && w->right->color == 'b'){
                w->color = 'r';
                x = x->p;
            }else{    
                if(w->right->color == 'b'){
                    w->left->color = 'b';
                    w->color = 'r';
                    _right_rotate(node, w);
                    w = x->p->right;
                }                
                w->color = x->p->color;
                x->p->color = 'b';
                w->right->color = 'b';
                _left_rotate(node,x->p);
                x = root;
            }
        }else{
            //lo mismo pero intercambiar right por left y viceverza
            struct s_node *w = x->p->left;
            if(w->color == 'r'){
                w->color = 'b';
                x->p->color = 'r';
                _right_rotate(node,x->p);
                w = x->p->left;
            }
            if(w->right->color == 'b' && w->left->color == 'b'){
                w->color = 'r';
                x = x->p;
            }else{    
                if(w->left->color == 'b'){
                    w->right->color = 'b';
                    w->color = 'r';
                    _left_rotate(node, w);
                    w = x->p->left;
                }                
                w->color = x->p->color;
                x->p->color = 'b';
                w->left->color = 'b';
                _right_rotate(node,x->p);
                x = root;
            }            
        }        
    }    
    x->color = 'b';    
}

void RBT::_rbt_insert(struct s_node *node, struct s_node *z){
    if( node == NULL){
        z->color = 'b';
        root = z;        
    }else{    
        struct s_node *y = node->p;
        struct s_node *x = node;
        while(x != NULL){
            y = x;
            if(z->key <= x->key)            
                x = x->left;
            else
                x = x->right;
        }           
        if (z->key <= y->key)
            y->left = z;
        else
            y->right = z;    
        
        z->p = y;    
        z->color = 'r';
        _insert_fixup(node,z);
    }
}

void RBT::_rbt_delete(struct s_node *node, struct s_node *delete_node){
    struct s_node *y = delete_node;
    struct s_node *x;
    char y_original_color = y->color;    
    if(delete_node->left == NULL){
        x = delete_node->right;
        _transplant(node,delete_node,delete_node->right);
    }else{
        if(delete_node->right == NULL){
            x = delete_node->left;
            _transplant(node,delete_node,delete_node->left);
        }else{
            y = _minimum(delete_node->right);
            y_original_color = y->color;
            x = y->right;
            if(y->p == delete_node)
                x->p = y;
            else{   
                _transplant(node,y,y->right);
                y->right = delete_node->right;
                y->right->p = y;
            }
            _transplant(node,delete_node,y);
            y->left = delete_node->left;
            y->left->p = y;
            y->color = delete_node->color;            
        }
    }    
    if(x!=NULL && y_original_color == 'b')
        _delete_fixup(node,x);
    free(delete_node);    
}       

void RBT::rbt_insert(int key){
    struct s_node *new_node;
    new_node = _new_node(key);
    // std::cout << "   Insert: " << key << std::endl;
    _rbt_insert(root,new_node);
}

void RBT::rbt_delete(int key){    
    struct s_node* delete_node = NULL;
    delete_node = _search(root, key);
    if(delete_node!=NULL){
        // std::cout << "   Delete: " << key << std::endl;
        _rbt_delete(root,delete_node);
    }else{
        // std::cout << "   Delete: " << key << "  NOT FOUND" << std::endl;
    }
}




int main(int argv, char **argc){

    // switch(argv){		
	// 	// case 2:
	// 	// 	if( strcmp(argc[1],"-h") == 0){
	// 	// 		disp_help();				
	// 	// 	}else{
	// 	// 		std::cout << "Los parametros son incorrectos. Ejecuta \"./output -h\" para abrir la ayuda" << std::endl;						
	// 	// 	}	
	// 	// 	exit(EXIT_FAILURE);		
	// 	case 4:			
	// 		break;
	// 	default:
	// 		std::cout << "ERROR, se esperan 3 parametros; 'BST' o 'RBT', n_keys, max_num" << std::endl;	
	// 		exit(EXIT_FAILURE);
	// }
    
    BST bst;     
    RBT rbt;         
    int option;
    uint32 n_keys, n_delete_keys, max_num, rand_key, rand_index;
    double elapsed_secs;
    clock_t begin, end;
    srand(time(NULL));   // Initialization, should only be called once.    
    
    max_num = atoll(argc[3]);    
    n_keys = atoll(argc[2]);            
    if(strcmp(argc[1],"BST")==0 || strcmp(argc[1],"RBT")==0){        
        if(strcmp(argc[1],"BST")==0)
            option = 1;
        else
            option = 2;
    }else{
        std::cout << "Se debe introducir 'BST' o 'RBT' como 1er parametro" << std::endl;	
		exit(EXIT_FAILURE);
    }    
    // option = 2;        
    // n_keys = 1000000;
    // max_num = 100;
    n_delete_keys = (int)(n_keys * .05);    
    uint32 *keys = (uint32 *)malloc(sizeof(uint32)*n_keys);
    uint32 *delete_keys = (uint32 *)malloc(sizeof(uint32)*n_delete_keys);
    // uint32 keys[n_keys];        
    // uint32 delete_keys[n_delete_keys]; 
    
    begin = clock();    
    if(option == 1){
        std::cout << "Inserting " << n_keys << " elements in BST" << std::endl;        
        keys[0] = max_num/2;   
        bst.insert(max_num/2);                
        for(uint32 i = 1; i<n_keys; i++){                    
            rand_key = rand()%max_num;
            keys[i] = rand_key;   
            bst.insert(rand_key);
        }  
    }else{    
        std::cout << "Inserting " << n_keys << " elements in RBT" << std::endl;
        for(uint32 i = 0; i<n_keys; i++){                    
            rand_key = rand()%max_num;
            keys[i] = rand_key;   
            rbt.rbt_insert(rand_key);            
        }
    }    
    end = clock();
    elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
    std::cout << "Elapsed Time in Insertion: " << elapsed_secs << std::endl;

    begin = clock();    
    if(option == 1){
        std::cout << "Searching " << n_delete_keys << " elements in BST" << std::endl;                    
        for(uint32 i = 0; i<n_delete_keys; i++){                                            
            do{
                rand_index = rand()%n_keys;
                rand_key = keys[rand_index];   
                delete_keys[i] = rand_key;              
                keys[rand_index] = -1;                 
            }while(rand_key != -1);            
            bst.search(rand_key);            
        }  
    }else{
        std::cout << "Searching " << n_delete_keys << " elements in RBT" << std::endl;                            
        for(uint32 i = 0; i<n_delete_keys; i++){                                
            do{
                rand_index = rand()%n_keys;
                rand_key = keys[rand_index];   
                delete_keys[i] = rand_key;              
                keys[rand_index] = -1;                 
            }while(rand_key != -1);           
            rbt.search(rand_key);                
        }  
    }
    end = clock();
    elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
    std::cout << "Elapsed Time in Search: " << elapsed_secs << std::endl;
    
    begin = clock();    
    if(option == 1){
        std::cout << "Deleating " << n_delete_keys << " elements in BST" << std::endl;                    
        for(uint32 i = 0; i<n_delete_keys; i++){                               
            bst.delete_(delete_keys[i]);            
        }  
    }else{
        std::cout << "Deleating " << n_delete_keys << " elements in RBT" << std::endl;                            
        for(uint32 i = 0; i<n_delete_keys; i++){                                                     
            rbt.rbt_delete(delete_keys[i]);            
        }  
    }
    end = clock();
    elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
    std::cout << "Elapsed Time in Deletion: " << elapsed_secs << std::endl;

}
